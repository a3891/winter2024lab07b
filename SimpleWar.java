public class SimpleWar {

    public static void main(String[] args) {

        Deck deck = new Deck();
        deck.shuffle();
        // System.out.println(deck); // Testing

        int player1 = 0;
        int player2 = 0;

        while (deck.length() > 0) { // Main game loop

            Card player1Card = deck.drawTopCard();
            Card player2Card = deck.drawTopCard();

            System.out.println("Player 1's card: " + player1Card);
            System.out.println("Score: " + player1Card.calculateScore());
            System.out.println("Player 2's card: " + player2Card);
            System.out.println("Score: " + player2Card.calculateScore());

            if (player1Card.calculateScore() > player2Card.calculateScore()) {
                player1++;
                System.out.println("Player 1 won this round.");
            } else if (player1Card.calculateScore() < player2Card.calculateScore()) {
                player2++;
                System.out.println("Player 2 won this round.");
            } else {
                System.out.println("Tie!");
            }

            System.out.println("Player 1 score: " + player1);
            System.out.println("Player 2 score: " + player2);
        }

        // Results
        if (player1 > player2) {
            System.out.println("Player 1 won the game!");
        } else if (player1 < player2) {
            System.out.println("Player 2 won the game!");
        } else {
            System.out.println("Tie!");
        }
    }
}

