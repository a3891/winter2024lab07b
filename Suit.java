public enum Suit {
    HEARTS(0.4),
    CLUBS(0.1),
    DIAMONDS(0.2),
    SPADES(0.3);

    private final double score;

    Suit(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }
}