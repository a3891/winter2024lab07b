import java.util.Random;

public class Deck {
    private final Card[] cards;
    private int numberOfCards;
    private final Random rng;

    public Deck() {
        rng = new Random();
        numberOfCards = 52;
        cards = new Card[numberOfCards];

        int index = 0;
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cards[index] = new Card(suit, rank);
                index++;
            }
        }
    }

    public int length() {
        return numberOfCards;
    }

    public Card drawTopCard() {
        Card drawnCard = cards[numberOfCards - 1];
        numberOfCards--;
        return drawnCard;
    }

    public void shuffle() {
        for (int i = 0; i < numberOfCards; i++) {
            int randomIndex = i + rng.nextInt(numberOfCards - i);
            // Swap the current card with the randomly chosen card
            Card temp = cards[i];
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }
}

